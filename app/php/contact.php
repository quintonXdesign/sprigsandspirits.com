<?php
require 'vendor/autoload.php';

// Register API keys at https://www.google.com/recaptcha/admin
$siteKey = '6LcerB8TAAAAAJUMAJPCFEzv-iPPVwXtU3Fgwhry';
$secret = ' 6LcerB8TAAAAAEUQigMfR4LSwLt6e4xKEgKDn_Em';
// reCAPTCHA supported 40+ languages listed here: https://developers.google.com/recaptcha/docs/language
$lang = 'en';


// Dotenv::load(__DIR__);

$sendgrid_username = 'xdesigninc';
$sendgrid_password = '5C071Z483201o407';


$from = $_POST['email'];

//form vars
$personsName = $_POST['name'];
$personsBusiness = $_POST['business'];
$personsAddress = $_POST['address'];
$personsCity = $_POST['city'];
$personsState = $_POST['state'];
$personsZip = $_POST['zip'];
$personsPhone = $_POST['phone'];
$personsSalesperson = $_POST['salesperson'];

$personsCocktail = $_POST['cocktail'];
$personsIngredients = $_POST['ingredients'];
$personsCCPItems = $_POST['ccp-items'];
$personsDirections = $_POST['directions'];
$personsGarnish = $_POST['garnish'];
$personsIce = $_POST['ice'];
$personsGlass = $_POST['glass'];
$personsStory = $_POST['story'];
// $personsName = $_POST['name'];
// $personsPhone = $_POST['phone'];
// $personsHistory = $_POST['history'];

$filePath = dirname(__FILE__) .'/../drink/uploads';
$uploaddir = dirname(__DIR__) .'/drink/uploads/';
$uploadfile = $uploaddir . basename($_FILES['drink-pic']['name']);
$finalPic = base64_encode(file_get_contents($_FILES['drink-pic']['tmp_name']));
$namePic = $_FILES['drink-pic']['name'];
$mimePic = $_FILES['drink-pic']['type'];
$emailSubject = 'Cocktail Submission from ' . $personsName;

// $messagePlain = ;

$message = '<p><strong>Name: </strong>' . $personsName . '<p>
      <p><strong>Email: </strong>' . $from . '<p>
      <p><strong>Business: </strong>' . $personsBusiness . '</p> 
      <p><strong>Address: </strong>' . $personsAddress . '</p> 
      <p><strong>City: </strong>' . $personsCity . '</p> 
      <p><strong>State: </strong>' . $personsState . '</p> 
      <p><strong>Zip: </strong>' . $personsZip . '</p> 
      <p><strong>Phone: </strong>' . $personsPhone . '</p> 
      <p><strong>Your Capitol City Procuce Salesperson: </strong>' . $personsSalesperson . '</p> 

      <p><strong>Name of Cocktail: </strong>' . $personsCocktail . '</p> 
      <p><strong>Ingredients (Specify brand and fluid ounces): </strong>' . $personsIngredients . '</p> 
      <p><strong>Specific Capitol City Produce Items Used: </strong>' . $personsCCPItems . '</p> 
      <p><strong>Directions: </strong>' . $personsDirections . '</p> 
      <p><strong>Garnish: </strong>' . $personsGarnish . '</p> 
      <p><strong>Type of Ice: </strong>' . $personsIce . '</p> 
      <p><strong>Type of Glass: </strong>' . $personsGlass . '</p> 
      <p><strong>Story of the Cocktail (Max 100 Words): </strong>' . $personsStory . '</p> 
      
        ';

$sendgrid = new SendGrid($sendgrid_username, $sendgrid_password, array("turn_off_ssl_verification" => true));


$email = new SendGrid\Email();
// $emails = array('britteny@thinkx.net','quinton@thinkx.net');
$emails = array('ahudnall@capitolcityproduce.com','hunter@thinkx.net','quinton@thinkx.net');

$email->setTos($emails)->
       setFrom($from)->
       setSubject($emailSubject)->
       setText(' ')->
       setAttachment($_FILES['drink-pic']['tmp_name'], $_FILES['drink-pic']['name'])->
       setHtml($message)->
       addHeader('X-Sent-Using', 'SendGrid-API')->
       addHeader('X-Transport', 'web');


// var_dump($email); die();

$response = $sendgrid->send($email);

// $recaptcha = new \ReCaptcha\ReCaptcha($secret);
// $resp = $recaptcha->verify($gRecaptchaResponse, $remoteIp);
// if ($resp->isSuccess()) {
//     // verified!
//   $response = $sendgrid->send($email);
// } else {
//     $errors = $resp->getErrorCodes();
// }

// $response = $sendgrid->send($email);

// print_r($response);
header('Location: /thank-you.html'); 
exit();